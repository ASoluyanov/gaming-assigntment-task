package com.gaming.bigdata.takehomeassignment.config

import ApplicationConfig._
import com.typesafe.config.{Config, ConfigFactory}

import scala.util.{Failure, Success, Try}

case class ApplicationConfig(input: InputConfig, output: OutputConfig, datasets: Map[String, DatasetConfig])

object ApplicationConfig {

  import io.circe.config.syntax._
  import io.circe.generic.auto._

  def read(): Try[ApplicationConfig] =
    Try {
      ConfigFactory
        .parseResources("application.conf")
        .getConfig("gaming.transactions.application")
        .resolve()
    }.flatMap {
        _.as[ApplicationConfig].fold(Failure(_), Success(_))
      }
      .recoverWith {
        case e => Failure(new IllegalArgumentException(s"Cannot read configuration for transformations", e))
      }

  case class InputConfig(location: String)

  case class OutputConfig(location: String)

  case class DatasetConfig(location: String)

}
