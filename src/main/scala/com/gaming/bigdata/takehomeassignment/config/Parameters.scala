package com.gaming.bigdata.takehomeassignment.config

import scopt.OptionParser

import scala.util.{Failure, Success, Try}

case class Parameters(transformation: String = "")

object Parameters {

  def parse(args: Array[String]): Try[Parameters] = {

    new OptionParser[Parameters]("game transactions transformation") {

      opt[String]('t', "transformation")
        .required()
        .valueName("<transformation>")
        .action((x, c) => c.copy(transformation = x))
        .text("Transformation")
    }.parse(args, Parameters()) match {
      case Some(x) => Success(x)
      case None =>
        Failure(new IllegalArgumentException("Error parsing arguments : " + args.mkString("[", ", ", "]")))
    }
  }
}
