package com.gaming.bigdata.takehomeassignment.config

import TransformationConfig._
import com.typesafe.config.ConfigFactory

import scala.util.{Failure, Success, Try}

case class TransformationConfig(select: List[Map[String, String]], mappings: List[MappingConfig], cleanup: List[String], rename: List[Map[String, String]])

object TransformationConfig {

  import io.circe.config.syntax._
  import io.circe.generic.auto._

  def read(outputTableName: String): Try[TransformationConfig] =
    Try {
      ConfigFactory
        .parseResources(s"$outputTableName.conf")
        .getConfig("gaming.transactions.transformations")
        .resolve()
    }.flatMap {
        _.as[TransformationConfig].fold(Failure(_), Success(_))
      }
      .recoverWith {
        case e => Failure(new IllegalArgumentException(s"Cannot read configuration for transformations", e))
      }

  case class MappingConfig(
      dataset: String,
      transformDataset: Option[Boolean],
      joinExpr: String,
      columns: Map[String, String],
      filterByIngestDate: Option[Boolean])


}
