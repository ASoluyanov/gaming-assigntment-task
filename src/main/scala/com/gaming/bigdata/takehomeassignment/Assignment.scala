package com.gaming.bigdata.takehomeassignment

import com.gaming.bigdata.takehomeassignment.config.ApplicationConfig.{DatasetConfig, InputConfig}
import com.gaming.bigdata.takehomeassignment.config.TransformationConfig.MappingConfig
import com.gaming.bigdata.takehomeassignment.config.{ApplicationConfig, Parameters, TransformationConfig}
import com.gaming.bigdata.takehomeassignment.config.TransformationConfig
import org.apache.spark.sql.{DataFrame, SparkSession}
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.functions.{expr, regexp_replace}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.col

import scala.util.Try

object Assignment extends App with LazyLogging {

  implicit val spark: SparkSession = SparkSession.builder().appName("game transactions transformation").getOrCreate()

  val process = for {
    parameters <- Parameters.parse(args)
    applicationConfig <- ApplicationConfig.read()
    transformationConfig <- TransformationConfig.read(parameters.transformation)
    inputDf <- readInputFile(applicationConfig.input)
    _ <- parameters.transformation match {
      case "fact_bet" => create_fact_bet(applicationConfig, transformationConfig, inputDf)
      case "dim_game" => create_dim_game(applicationConfig, transformationConfig, inputDf)
      case "dim_player" => create_dim_player(applicationConfig, transformationConfig, inputDf)
      case _ => throw new Exception("wrong parameter")
    }
  } yield {
    logger.info("Transformation executed.")
  }

  def readInputFile(inputConfig: InputConfig)(implicit spark: SparkSession): Try[DataFrame] = Try {

    spark.read
      .format("csv")
      .option("sep", ";")
      .option("inferSchema", "true")
      .option("header", "true")
      .load(inputConfig.location)
      .withColumn("realAmount", regexp_replace(col("realAmount"), ",", ".").cast("double"))
      .withColumn("bonusAmount", regexp_replace(col("bonusAmount"), ",", ".").cast("double"))
  }

  def create_fact_bet(
      applicationConfig: ApplicationConfig,
      transformationConfig: TransformationConfig,
      inputDf: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {

    /**
      * This method defines the date when each record in the player reference data set stops being valid.
      * Results are stored in EffectiveTo column
      */
    def updatePlayerDf: DataFrame => DataFrame = (playerDf: DataFrame) => {
      val windowSpec = Window.partitionBy("playerID").orderBy(desc("latestUpdate"))
      playerDf
        .withColumn("effectiveTo", lag("latestUpdate", 1).over(windowSpec))
        .select("playerId", "country", "latestUpdate", "effectiveTo")
    }

    val aggregatedDf =
      inputDf.groupBy("PlayerId", "gameID", "Date", "txCurrency").pivot("txType").sum("realAmount", "bonusAmount")

    val transformedDf = transformationConfig.select
      .foldLeft(aggregatedDf) { (df, column) =>
        df.withColumn(column.head._1.toLowerCase, expr(column.head._2))
      }

    val mappedDf = transformationConfig.mappings
      .foldLeft(transformedDf) { (df, mapping) =>
        mapColumns(df, mapping, applicationConfig.datasets(mapping.dataset), Option(updatePlayerDf))
      }

    transformationConfig.rename
      .foldLeft(mappedDf) { (df, column) =>
        df.withColumnRenamed(column.head._2.toLowerCase, column.head._1.toLowerCase)
      }
      .select(transformationConfig.cleanup.map(col): _*)
  }

  def create_dim_game(
      applicationConfig: ApplicationConfig,
      transformationConfig: TransformationConfig,
      inputDF: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {

    val transformedDf = inputDF.select(expr(transformationConfig.select.head.head._2)).distinct()

    val mappedDf = transformationConfig.mappings
      .foldLeft(transformedDf) { (df, mapping) =>
        mapColumns(df, mapping, applicationConfig.datasets(mapping.dataset), None)
      }
    transformationConfig.rename
      .foldLeft(mappedDf) { (df, column) =>
        df.withColumnRenamed(column.head._2.toLowerCase, column.head._1.toLowerCase)
      }
  }

  def create_dim_player(
      applicationConfig: ApplicationConfig,
      transformationConfig: TransformationConfig,
      inputDf: DataFrame)(implicit spark: SparkSession): Try[DataFrame] = Try {

    /**
      * This method filters only valid player records
      */
    def updatePlayerDf: DataFrame => DataFrame = (playerDf: DataFrame) => {
      val windowSpec = Window.partitionBy("playerId").orderBy("latestUpdate")
      playerDf
        .withColumn("rank", rank().over(windowSpec))
        .filter("rank = 1")
        .select("gender", "country", "latestUpdate", "playerid")
    }

    val cleanInputDf = inputDf.select("playerid").distinct()

    val mappedDf = transformationConfig.mappings
      .foldLeft(cleanInputDf) { (df, mapping) =>
        mapColumns(df, mapping, applicationConfig.datasets(mapping.dataset), Some(updatePlayerDf))
      }

    transformationConfig.rename
      .foldLeft(mappedDf) { (df, column) =>
        df.withColumnRenamed(column.head._2.toLowerCase, column.head._1.toLowerCase)
      }
  }

  /**
    * Method performs enrichment of input dataframe with the data from reference datasets. Only required columns specified
    * in config file are being added. If reference table transformation required, provide an option of function that performs this
    * transformation as updateLookupTable parameter and specify this requirement in configuration file.
    * If not, set this parameter to None.
    */
  private def mapColumns(
      input: DataFrame,
      mappingConfig: MappingConfig,
      datasetConfig: DatasetConfig,
      updateLookupTable: Option[DataFrame => DataFrame]
  )(implicit spark: SparkSession): DataFrame = {

    val rawLookupDf =
      spark.read
        .format("csv")
        .option("sep", ",")
        .option("inferSchema", "true")
        .option("header", "true")
        .load(datasetConfig.location)

    val lookupDf =
      if (mappingConfig.transformDataset.getOrElse(false)) {
        updateLookupTable match {
          case Some(f) => f(rawLookupDf)
          case None => throw new Exception(s"reference data set transformation function is not specified")
        }
      } else rawLookupDf

    val joinedDf = input.as("input").join(broadcast(lookupDf.as("lookup")), expr(mappingConfig.joinExpr), "left")

    val inputColumns = input.columns.map(_.toLowerCase).toList
    val mappedColumns = mappingConfig.columns.keys.map(_.toLowerCase).toList
    val outputColumns =
      inputColumns.filterNot(mappedColumns.contains).map(input(_)) :::
        mappingConfig.columns.map {
        case (alias, columnExpr) => expr(columnExpr).as(alias.toLowerCase)
      }.toList

    joinedDf.select(outputColumns: _*)

  }
}
