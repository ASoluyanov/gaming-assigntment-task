package com.gaming.bigdata.takehomeassignment

import com.gaming.bigdata.takehomeassignment.config.ApplicationConfig.{DatasetConfig, InputConfig}
import org.scalatest._
import com.holdenkarau.spark.testing.DataFrameSuiteBase
import com.gaming.bigdata.takehomeassignment.config.{ApplicationConfig, TransformationConfig}
import com.gaming.bigdata.takehomeassignment.config.TransformationConfig
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.types._

class AssignmentTest extends WordSpec with Matchers with DataFrameSuiteBase {

  "Assignment" should {

    val inputPath = getClass.getResource("/input_files/GameTransaction.csv").getPath
    val inputConfig = InputConfig(inputPath)

    "read input file " in {

      val inputPath = getClass.getResource("/input_files/GameTransaction.csv").getPath
      val inputConfig = InputConfig(inputPath)
      val inputDf = Assignment.readInputFile(inputConfig)(spark).get
      inputDf.show()
      inputDf.count shouldBe 38025
      inputDf.schema shouldBe StructType(
        Seq(
          StructField("Date", TimestampType),
          StructField("realAmount", DoubleType),
          StructField("bonusAmount", DoubleType),
          StructField("channelUID", StringType),
          StructField("txCurrency", StringType),
          StructField("gameID", IntegerType),
          StructField("txType", StringType),
          StructField("BetId", IntegerType),
          StructField("PlayerId", IntegerType)
        )
      )
    }

    "perform fact_bet transformation" in {
      val inputDf = Assignment.readInputFile(inputConfig)(spark).get
      val applicationConfig = ApplicationConfig
        .read()
        .get
        .copy(datasets = Map(
          "currency_exchange" -> DatasetConfig(getClass.getResource("/input_files/CurrencyExchange.csv").getPath),
          "player" -> DatasetConfig(getClass.getResource("/input_files/Player.csv").getPath)
        ))
      val transformationConfig = TransformationConfig.read("fact_bet").get
      val outputDf: DataFrame = Assignment.create_fact_bet(applicationConfig, transformationConfig, inputDf)(spark).get
      outputDf.show
      outputDf
        .filter("player_id = 21 AND date >= '2017-03-29'")
        .select("country")
        .distinct
        .collect should contain theSameElementsAs Seq(Row("AT"))

      outputDf
        .filter("player_id = 21 AND date < '2017-03-29'")
        .select("country")
        .distinct
        .collect should contain theSameElementsAs Seq(Row("SE"))

      outputDf.schema shouldBe StructType(
        Seq(
          StructField("date", TimestampType),
          StructField("player_id", IntegerType),
          StructField("country", StringType),
          StructField("game_id", IntegerType),
          StructField("Cash turnover", DoubleType),
          StructField("Bonus turnover", DoubleType),
          StructField("Cash winnings", DoubleType),
          StructField("Bonus winnings", DoubleType),
          StructField("Turnover", DoubleType),
          StructField("Winnings", DoubleType),
          StructField("Cash result", DoubleType),
          StructField("Bonus result", DoubleType),
          StructField("Gross result", DoubleType)
        )
      )
    }

    "perform dim_game transformation" in {
      val inputDf = Assignment.readInputFile(inputConfig)(spark).get
      val applicationConfig = ApplicationConfig
        .read()
        .get
        .copy(datasets = Map(
          "game" -> DatasetConfig(getClass.getResource("/input_files/Game.csv").getPath),
          "game_category" -> DatasetConfig(getClass.getResource("/input_files/GameCategory.csv").getPath),
          "game_provider" -> DatasetConfig(getClass.getResource("/input_files/GameProvider.csv").getPath)
        ))
      val transformationConfig = TransformationConfig.read("dim_game").get
      val outputDf = Assignment.create_dim_game(applicationConfig, transformationConfig, inputDf)(spark).get
      outputDf.show()
      outputDf.schema shouldBe StructType(
        Seq(
          StructField("game_id", IntegerType),
          StructField("game_name", StringType),
          StructField("game_category", StringType),
          StructField("provider_name", StringType)
        )
      )
      outputDf.count() shouldBe 134
      outputDf.filter("game_id = 28").collect() should contain theSameElementsAs Seq(
        Row(28, "CYNTHIA", "Slots", "Lifo games"))
    }

    "perform dim_player transformation" in {
      val inputDf = Assignment.readInputFile(inputConfig)(spark).get
      val applicationConfig = ApplicationConfig
        .read()
        .get
        .copy(
          datasets = Map(
            "player" -> DatasetConfig(getClass.getResource("/input_files/Player.csv").getPath)
          ))
      val transformationConfig = TransformationConfig.read("dim_player").get
      val outputDf = Assignment.create_dim_player(applicationConfig, transformationConfig, inputDf)(spark).get
      outputDf.show()
      outputDf.schema shouldBe StructType(
        Seq(
          StructField("player_id", IntegerType),
          StructField("latestupdate", TimestampType),
          StructField("gender", StringType),
          StructField("country", StringType)
        )
      )
      outputDf
        .filter("player_id = 21 OR player_id = 11 OR player_id = 45")
        .withColumn("latestupdate", col("latestupdate").cast(StringType))
        .collect should contain theSameElementsAs Seq(
        Row(11, "2017-01-01 00:00:00", "FEMALE", "SE"),
        Row(45, "2017-01-01 00:00:00", "MALE", "NO"),
        Row(21, "2017-01-01 00:00:00", "MALE", "SE")
      )
    }
  }
}
