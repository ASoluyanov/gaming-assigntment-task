Application requires only one parameter -transformation. Possible values are:
"fact_bet", "dim_game", "dim_player". Input files locations should be specified
in application.conf file.

Provided test files are located in test resources.

To check the output you can run test classes.
